# XML parser

XML parser is a Java application for dealing with xml documents.

## Installation

Use the package manager [maven](https://maven.apache.org/) to build XML parser.

```bash
mvn clean package
```

## Usage

```bash
java -jar ./target/xml-parser-0.0.1-SNAPSHOT.jar
```