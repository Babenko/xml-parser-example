package com.demo.xmlparser.it;

import com.demo.xmlparser.dao.UserRepository;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

@SpringBootTest(properties = {"path.xml.file=classpath:xml/Java_test.003.xml"})
class ApplicationXmlParserApplicationIT {

    @Autowired
    private UserRepository userRepository;

    @Test
    void shouldFailProcessingWhenXmlIsIncorrect() throws InterruptedException {
        Awaitility.await()
                .timeout(1500, TimeUnit.MILLISECONDS)
                .during(1000, TimeUnit.MILLISECONDS)
                .pollInterval(250, TimeUnit.MILLISECONDS)
                .until(() -> userRepository.findAll().size() == 0);
    }
}
