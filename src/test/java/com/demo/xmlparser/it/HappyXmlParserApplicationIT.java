package com.demo.xmlparser.it;

import com.demo.xmlparser.dao.UserRepository;
import com.demo.xmlparser.model.entity.User;
import org.awaitility.Awaitility;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.TimeUnit;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@SpringBootTest
class HappyXmlParserApplicationIT {

    private static final int NUMBER_OF_USERS = 3;

    @Autowired
    private UserRepository userRepository;

    @Test
    void shouldParseXMLFile() {
        Awaitility.await()
                .timeout(1000, TimeUnit.MILLISECONDS)
                .pollInterval(250, TimeUnit.MILLISECONDS)
                .until(this::hasReachMaximumNumberOfUsers);

        User user = userRepository.findByInn(1234567890L).orElse(null);

        assertThat(user, notNullValue());
        assertThat(user.getFirstName(), equalTo("Ivan"));
        assertThat(user.getLastName(), equalTo("Ivanoff"));
        assertThat(user.getMiddleName(), equalTo("Ivanoff"));
    }

    private boolean hasReachMaximumNumberOfUsers() {
        return userRepository.findAll().size() == NUMBER_OF_USERS;
    }
}
