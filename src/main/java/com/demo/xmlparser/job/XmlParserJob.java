package com.demo.xmlparser.job;

import com.demo.xmlparser.job.handler.JacksonParserHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.Resource;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;

@Slf4j
@Component
public class XmlParserJob implements BaseJob {

    private Resource xmlFile;
    private JacksonParserHandler xmlParserHandler;

    public XmlParserJob(@Value("${path.xml.file}") Resource xmlFile, JacksonParserHandler xmlParserHandler) {
        this.xmlFile = xmlFile;
        this.xmlParserHandler = xmlParserHandler;
    }

    @Async
    @Override
    public void process() {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser saxParser;
        try {
            saxParser = factory.newSAXParser();
            saxParser.parse(xmlFile.getInputStream(), xmlParserHandler);
        } catch (ParserConfigurationException | SAXException | IOException e) {
            log.error("Xml parser issue", e);
        }

    }
}
