package com.demo.xmlparser.job.handler;

import com.demo.xmlparser.model.aggregate.UserTransaction;
import com.demo.xmlparser.model.event.Event;
import com.demo.xmlparser.model.event.PoisonEvent;
import com.demo.xmlparser.model.event.XmlEvent;
import com.fasterxml.jackson.databind.ObjectReader;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.io.IOException;
import java.util.Queue;

@Slf4j
@Primary
@Component
@RequiredArgsConstructor
public class JacksonParserHandler extends DefaultHandler {

    private static final String TRANSACTIONAL_TAG = "transaction";
    private StringBuilder rawTransactionContent = new StringBuilder(1024);
    private final Queue<Event<UserTransaction>> queue;
    private final ObjectReader reader;

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String content = new String(ch, start, length);
        if (StringUtils.hasText(content)) {
            rawTransactionContent.append(content);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        if (isTransactionalElement(qName)) {
            rawTransactionContent.setLength(0);
        }
        appendOpenTag(qName);
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        try {
            appendCloseTag(qName);
            if (isTransactionalElement(qName)) {
                UserTransaction userTransaction = reader.readValue(rawTransactionContent.toString(), UserTransaction.class);
                queue.add(new XmlEvent(userTransaction));
            }
        } catch (IOException e) {
            log.error("Can't parse user transaction record.", e);
        }
    }

    @Override
    public void endDocument() throws SAXException {
        queue.add(new PoisonEvent());
    }

    private boolean isTransactionalElement(String element) {
        return TRANSACTIONAL_TAG.equals(element);
    }

    private void appendOpenTag(String tag) {
        rawTransactionContent.append("<").append(tag).append(">");
    }

    private void appendCloseTag(String tag) {
        rawTransactionContent.append("</").append(tag).append(">");
    }
}
