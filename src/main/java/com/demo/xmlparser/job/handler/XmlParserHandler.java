package com.demo.xmlparser.job.handler;

import com.demo.xmlparser.model.aggregate.UserTransaction;
import com.demo.xmlparser.model.dto.User;
import com.demo.xmlparser.model.event.Event;
import com.demo.xmlparser.model.event.PoisonEvent;
import com.demo.xmlparser.model.event.XmlEvent;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Queue;
import java.util.stream.Stream;

@Component
@RequiredArgsConstructor
public class XmlParserHandler extends DefaultHandler {

    private static final String USER_TAG = "client";
    private static final String TRANSACTIONAL_TAG = "transaction";
    private final Queue<Event<UserTransaction>> queue;
    private Map<String, String> userValues = new HashMap<>();
    private Map<String, String> userTransactionsValues = new HashMap<>();
    private String lastQName;
    private User lastUser;

    @Override
    public void startDocument() throws SAXException {
        super.startDocument();
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        String value = new String(ch, start, length);
        if (StringUtils.hasText(value)) {
            this.userTransactionsValues.put(lastQName, value);
            this.userValues.put(lastQName, value);
        }
    }

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.lastQName = qName;
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        super.endElement(uri, localName, qName);
        if (USER_TAG.equals(qName)) {
            lastUser = construct(User.class);
        } else if (TRANSACTIONAL_TAG.equals(qName)) {
            UserTransaction userTransactional = construct(UserTransaction.class);
            queue.add(new XmlEvent(userTransactional));
        }
    }

    @Override
    public void endDocument() throws SAXException {
        queue.add(new PoisonEvent());
    }

    @SneakyThrows
    private <T> T construct(Class<T> clazz) {
        T obj = clazz.newInstance();
        Stream.of(clazz.getDeclaredFields())
                .forEach(field -> setFiled(field, userValues.get(field.getName()), obj));
        return obj;
    }

    @SneakyThrows
    private void setFiled(Field field, String value, Object target)  {
        field.setAccessible(true);
        field.set(target, cast(value, field.getType()));
    }

    private Object cast(String value, Class clazz) {
        switch (clazz.getTypeName()) {
            case "java.lang.Long":
                return Long.valueOf(value);
            case "java.math.BigDecimal":
                return new BigDecimal(value);
            case "com.demo.xmlparser.model.dto.User":
                return lastUser;
            default:
                return value;
        }
    }
}
