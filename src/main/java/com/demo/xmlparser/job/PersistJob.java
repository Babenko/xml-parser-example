package com.demo.xmlparser.job;

import com.demo.xmlparser.model.aggregate.UserTransaction;
import com.demo.xmlparser.model.dto.Tuple;
import com.demo.xmlparser.model.entity.Transaction;
import com.demo.xmlparser.model.entity.User;
import com.demo.xmlparser.model.event.Event;
import com.demo.xmlparser.model.event.EventType;
import com.demo.xmlparser.model.mapper.TransactionalMapper;
import com.demo.xmlparser.model.mapper.UserMapper;
import com.demo.xmlparser.service.UserTransactionService;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;
import reactor.core.publisher.FluxSink;

import java.util.Objects;
import java.util.Queue;

@Slf4j
@Component
@AllArgsConstructor
public class PersistJob implements BaseJob {

    private Queue<Event<UserTransaction>> queue;
    private TransactionalMapper transactionalMapper;
    private UserMapper userMapper;
    private UserTransactionService userTransactionService;

    @Async
    @Override
    public void process() {
        Flux.create(this::populateXmlEvent)
                .map(Event::getPayload)
                .map(this::map)
                .log()
                .subscribe(this::save);

        log.debug("All users={}", userTransactionService.getAllUsers());
    }

    public void save(Tuple<User, Transaction> userTransactionTuple) {
        userTransactionService.saveUserTransaction(userTransactionTuple);
    }

    @SneakyThrows
    private void populateXmlEvent(FluxSink<Event<UserTransaction>> emitter) {
        while (true) {
            Event<UserTransaction> event = queue.poll();
            if (Objects.isNull(event)) {
                continue;
            }
            if (event.getEventType().equals(EventType.POISON)) {
                return;
            }
            emitter.next(event);
        }
    }

    private Tuple<User, Transaction> map(UserTransaction userTransaction) {
        Transaction transaction = transactionalMapper.map(userTransaction);
        User user = userMapper.map(userTransaction.getUser());
        return new Tuple<>(user, transaction);
    }

}
