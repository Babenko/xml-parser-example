package com.demo.xmlparser.runner;

import com.demo.xmlparser.job.BaseJob;
import com.demo.xmlparser.job.XmlParserJob;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

@Component
public class XmlParserRunner implements ApplicationRunner {

    private BaseJob xmlParserJob;

    public XmlParserRunner(@Qualifier("xmlParserJob") BaseJob xmlParserJob) {
        this.xmlParserJob = xmlParserJob;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        xmlParserJob.process();
    }
}
