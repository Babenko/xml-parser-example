package com.demo.xmlparser.runner;

import com.demo.xmlparser.job.BaseJob;
import com.demo.xmlparser.job.PersistJob;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
public class PersistRunner implements ApplicationRunner {

    private BaseJob persistJob;

    public PersistRunner(@Qualifier("persistJob") BaseJob persistJob) {
        this.persistJob = persistJob;
    }

    @Override
    public void run(ApplicationArguments args) throws Exception {
        persistJob.process();
    }
}
