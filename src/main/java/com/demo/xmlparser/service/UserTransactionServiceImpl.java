package com.demo.xmlparser.service;

import com.demo.xmlparser.dao.TransactionRepository;
import com.demo.xmlparser.dao.UserRepository;
import com.demo.xmlparser.model.dto.Tuple;
import com.demo.xmlparser.model.entity.Transaction;
import com.demo.xmlparser.model.entity.User;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
@AllArgsConstructor
public class UserTransactionServiceImpl implements UserTransactionService {

    private UserRepository userRepository;
    private TransactionRepository transactionRepository;

    @Override
    @Transactional
    public void saveUserTransaction(Tuple<User, Transaction> userTransactionTuple) {
        User user = userRepository.save(userTransactionTuple.getFirst());
        Transaction transaction = userTransactionTuple.getSecond();
        transaction.setUser(user);
        transactionRepository.save(transaction);
    }

    @Override
    public List<User> getAllUsers() {
        return userRepository.findAll();
    }

    @Override
    public List<Transaction> getAllTransactions() {
        return transactionRepository.findAll();
    }
}
