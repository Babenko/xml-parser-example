package com.demo.xmlparser.service;


import com.demo.xmlparser.model.aggregate.UserTransaction;
import com.demo.xmlparser.model.dto.Currency;
import com.demo.xmlparser.model.dto.User;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.github.javafaker.Faker;
import lombok.SneakyThrows;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.locks.LockSupport;
import java.util.stream.Collectors;
import java.util.stream.LongStream;

public class XmlGeneratorService {

    private static final XmlMapper xmlMapper = new XmlMapper();

    static ObjectWriter mapper = xmlMapper.writerFor(UserTransaction.class);

    private static AtomicInteger atomicInteger = new AtomicInteger(0);

    public static void main(String[] args) throws IOException {

//        Faker instance = Faker.instance();
//        String collect = LongStream.range(1, 5_000_000)
//                .parallel()
//                .mapToObj(id -> new User(instance.name().firstName(), instance.name().lastName(), instance.name().nameWithMiddle(), id))
//                .map(user -> new UserTransaction(instance.address().cityName(), new BigDecimal(instance.number().digit()), Currency.UAH, instance.finance().creditCard(), user))
//                .map(XmlGeneratorService::toXML)
//                .peek(string -> showCounter(atomicInteger))
//                .collect(Collectors.joining("\n"));
//
//        File file = new File("F:\\123\\transactions.xml");
//        Files.write(Paths.get(file.getAbsolutePath()), collect.getBytes());
    }


    private static void showCounter(AtomicInteger counter) {
        if (counter.incrementAndGet() % 100_000 == 0) {
            System.out.println(counter.get());
        }
    }

    @SneakyThrows
    private static String toXML(UserTransaction userTransaction) {
        return mapper.writeValueAsString(userTransaction);
    }

}
