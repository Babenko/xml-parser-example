package com.demo.xmlparser.service;

import org.springframework.context.ApplicationEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

@Component
public class Listener {

    @EventListener
    public void eventHandler(ApplicationEvent event) {
        System.out.println("event - " + event);
    }
}
