package com.demo.xmlparser.service;

import com.demo.xmlparser.model.dto.Tuple;
import com.demo.xmlparser.model.entity.Transaction;
import com.demo.xmlparser.model.entity.User;
import org.springframework.stereotype.Service;

import java.util.List;


public interface UserTransactionService {

    void saveUserTransaction(Tuple<User, Transaction> userTransactionTuple);

    List<User> getAllUsers();

    List<Transaction> getAllTransactions();
}
