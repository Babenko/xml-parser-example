package com.demo.xmlparser.model.event;

import com.demo.xmlparser.model.aggregate.UserTransaction;
import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class XmlEvent implements Event<UserTransaction> {

    private UserTransaction userTransaction;

    @Override
    public EventType getEventType() {
        return EventType.XML;
    }

    @Override
    public UserTransaction getPayload() {
        return userTransaction;
    }
}
