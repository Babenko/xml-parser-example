package com.demo.xmlparser.model.event;

public interface Event<T> {
    EventType getEventType();
    T getPayload();
}
