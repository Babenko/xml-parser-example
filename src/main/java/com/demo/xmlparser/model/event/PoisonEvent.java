package com.demo.xmlparser.model.event;

import com.demo.xmlparser.model.aggregate.UserTransaction;
import lombok.ToString;

@ToString
public class PoisonEvent implements Event<UserTransaction> {
    @Override
    public EventType getEventType() {
        return EventType.POISON;
    }

    @Override
    public UserTransaction getPayload() {
        return null;
    }
}
