package com.demo.xmlparser.model.mapper;

import com.demo.xmlparser.model.aggregate.UserTransaction;
import com.demo.xmlparser.model.dto.Currency;
import com.demo.xmlparser.model.entity.Transaction;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import java.math.BigDecimal;

@Mapper(componentModel = "spring")
public interface TransactionalMapper {
    @Mapping(source = "userTransaction", target = "amount", qualifiedByName = "toAmount")
    @Mapping(source = "userTransaction", target = "currency", qualifiedByName = "toCurrency")
    @Mapping(target = "user", ignore = true)
    Transaction map(UserTransaction userTransaction);

    @Named("toAmount")
    default String toAmount(UserTransaction userTransaction) {
        return userTransaction.getAmount().toString();
    }

    @Named("toCurrency")
    default String toCurrency(UserTransaction userTransaction) {
        return userTransaction.getCurrency();
    }
}
