package com.demo.xmlparser.model.mapper;

import com.demo.xmlparser.model.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface UserMapper {
   @Mapping(target = "transactions", ignore = true)
   User map(com.demo.xmlparser.model.dto.User user);
}
