package com.demo.xmlparser.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class User {
    @Id
    private Long inn;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, mappedBy = "user")
    private List<Transaction> transactions;
    @Column
    private String firstName;
    @Column
    private String lastName;
    @Column
    private String middleName;
}
