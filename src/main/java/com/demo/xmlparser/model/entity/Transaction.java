package com.demo.xmlparser.model.entity;

import lombok.*;

import javax.persistence.*;
import java.util.concurrent.locks.LockSupport;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Transaction {
    @Id
    @GeneratedValue
    private Long id;
    @ManyToOne(cascade = CascadeType.ALL)
    @JoinColumn(name="user_inn", nullable=false)
    private User user;
    private String place;
    private String amount;
    private String currency;
    private String card;

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", place='" + place + '\'' +
                ", amount='" + amount + '\'' +
                ", currency='" + currency + '\'' +
                ", card='" + card + '\'' +
                '}';
    }
}
