package com.demo.xmlparser.model.aggregate;

import com.demo.xmlparser.model.dto.Currency;
import com.demo.xmlparser.model.dto.User;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonRootName;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@JsonRootName("transaction")
public class UserTransaction {
    private String place;
    private BigDecimal amount;
	private String currency;
	private String card;
    @JsonProperty("client")
	private User user;
}
