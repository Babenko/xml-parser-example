package com.demo.xmlparser.model.dto;

public enum Currency {
    UAH,
    EUR,
    USD
}
