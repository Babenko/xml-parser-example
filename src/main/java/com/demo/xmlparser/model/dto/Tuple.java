package com.demo.xmlparser.model.dto;

import lombok.Data;

@Data
public class Tuple<F, S> {
    private final F first;
    private final S second;
}
