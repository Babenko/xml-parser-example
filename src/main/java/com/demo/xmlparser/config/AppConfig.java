package com.demo.xmlparser.config;

import com.demo.xmlparser.model.aggregate.UserTransaction;
import com.demo.xmlparser.model.event.Event;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;

@Configuration
public class AppConfig {

    @Bean
    public Queue<Event<UserTransaction>> queue(@Value("${event.queue.capacity}") Integer capacity) {
        return new ArrayBlockingQueue<>(capacity);
    }

    @Bean
    public ObjectReader userTransactionMapper() {
        return new XmlMapper().readerFor(UserTransaction.class);
    }
}
